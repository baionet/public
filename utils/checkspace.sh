#! /bin/bash
# from pizzacoca
# DEBIAN 10 (Buster), 9 (Stretch) or 8 (Jessie)
# script livré sans garantie etc.
# 24/01/2022 > encore du dev à faire (notemment pour les logs )

# gestion de l'affichage
function echo_part_menu() { echo -e "\e[0;32m${*}\e[m"; }
function echo_step_menu() { echo -e " \u2022 \e[0;36m${*}\e[m"; }
function echo_ok_menu() { echo -e "   \e[1;32m${*}\e[m"; }
function echo_ko_menu() { echo -e "   \e[1;31m${*}\e[m"; }

# Affichage archivé dans le log
function echo_part() { echo "=== $* ===" >> $LOG; echo -e "\e[0;32m${*}\e[m"; }
function echo_step() { echo "==> $* ==" >> $LOG; echo -e " \u2022 \e[0;36m${*}\e[m"; }
function echo_point() { echo "=> $* =" >> $LOG; echo -e "   \e[0;32m${*}\e[m"; }
function echo_ok() { echo "=> $* =" >> $LOG; echo -e "   \e[1;32m${*}\e[m"; }
function echo_ko() { echo "=> $* =" >> $LOG; echo -e "   \e[1;31m${*}\e[m"; }

# variables maintenance
rep_maintenance="$HOME/.config/baionet"
mkdir -p ${rep_maintenance}/logs
LOG=${rep_maintenance}/logs/checkspace.log

# Fonctions de nettoyage
function purge_paquets() {
	echo_part "##########################################"
	echo_part"nettoyage"
	echo_part "##########################################"
	apt autoclean
	apt clean
	apt --purge autoremove
}

function liste_images() {
	echo_part "##########################################"
	echo_part "liste des images "
	echo_part "dpkg --list | grep linux-image"
	echo_part "##########################################"
	dpkg --list | grep linux-image
}
function purge_kernels() {
	echo_part "##########################################"
	echo_part "delete old kernels"
	echo_part "##########################################"
	v="$(uname -r | awk -F '-virtual' '{ print $1}')"
	i="linux-headers-virtual|linux-image-virtual|linux-headers-${v}|linux-image-$(uname -r)"
	apt-get --purge remove $(dpkg --list | egrep -i 'linux-image|linux-headers' | awk '/ii/{ print $2}' | egrep -v "$i")
}

function liste_partitions() {
	echo_part "##########################################"
	echo_part "liste des partitions"
	echo_part "sfdisk -l"
	echo_part "##########################################"
	sfdisk -l
	
	echo_part "##########################################"
	echo_part "utilisation des partitions"
	echo_part "df -lhT"
	echo_part "##########################################"
	df -lhT
}

function liste_dossiers() {
		array=("/var" "/tmp" "/home") 

	echo_part "##########################################"
	echo_part "répertoires sous /var"
	echo_part "du -a /var | sort -nr | head -n 10"
	echo_part "##########################################"
	du -a /var | sort -nr | head -n 10

	echo_part "##########################################"
	echo_part "répertoires sous /tmp"
	echo_part "du -a /tmp | sort -nr | head -n 10"
	echo_part "##########################################"
	du -a /tmp | sort -nr | head -n 10

	echo_part "##########################################"
	echo_part "répertoires sous /home"
	echo_part "du -a /hom | sort -nr | head -n 10"
	echo_part "##########################################"
	du -a /home | sort -nr | head -n 10


	echo_part "##########################################"
	echo_part "taille totale"
	echo_part "/usr/bin/du --total --summarize --human-readable --one-file-system"
	echo_part "##########################################"
	cd /
	/usr/bin/du --total --summarize --human-readable --one-file-system
	
}

function liste_paquets() {
	echo_part "##########################################"
	echo_part "Installed package by size"
	echo_part "dpkg-query -W -show-format='${Installed-Size} ${Package}_n' | sort -n | tail"
	echo_part "##########################################"
	dpkg-query -W --showformat='${Installed-Size} ${Package}\n' | sort -n | tail 
}

# Affichage de départ
function help() {
#clear	

	echo_step "...menu..."
	# Affichage de l'aide
    echo_part_menu "\nConfiguration script for Debian linux system"
    echo_part_menu "GPLv3\e[0;36m and above"
    echo_part_menu "Pizzacoca 2021"

    echo_part_menu "\nHelp (checkspace.sh)"

    echo_step_menu "liste_partitions :\e[m Lister les partitions"
    echo_step_menu "liste_dossiers   :\e[m Lister les dossiers les plus gros"
    echo_step_menu "liste_paquets    :\e[m Lister les paquets les plus gros"
    echo_step_menu "liste_images     :\e[m LIste des images présentes"
    echo_step_menu "purge_paquets    :\e[m Nettoyage apt clean etc."
    echo_step_menu "purge_kernels     :\e[m Nettoyage des anciens kernels"

    echo_ko_menu "\nCe script est à lancer avec les droits root"
    echo_ko_menu "exemple :\e[m ./checkspace.sh liste_images"

    
	echo_part_menu "\nLogs consultables sous $LOG"
} #help
	
[ $# -eq 0 ] && help
echo -e "Configure : $*" >> $LOG
echo -e "$(date)" >> $LOG

for RUN in $*
do
  echo -e "\n[ $RUN ]" >> $LOG
  $(declare -f -F $RUN)
done

echo -e "\nDone." >> $LOG
