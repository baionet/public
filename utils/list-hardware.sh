#!/bin/bash

inxi -A -B -C -D -G -M -I > hardware.log
lscpu >> hardware.log
lspci >> hardware.log
lsusb >> hardware.log
lsmem >> hardware.log

