#!/bin/bash
# From pizzacoca
# Configuration script for :
# DEBIAN 10 (Buster), 9 (Stretch) or 8 (Jessie)
# 24/01/2022 > encore en cours de dev !

# gestion de l'affichage
function echo_part_menu() { echo -e "\e[0;32m${*}\e[m"; }
function echo_step_menu() { echo -e " \u2022 \e[0;36m${*}\e[m"; }
function echo_ok_menu() { echo -e "   \e[1;32m${*}\e[m"; }
function echo_ko_menu() { echo -e "   \e[1;31m${*}\e[m"; }

# Affichage archivé dans le log
function echo_part() { echo "=== $* ===" >> $LOG; echo -e "\e[0;32m${*}\e[m"; }
function echo_step() { echo "==> $* ==" >> $LOG; echo -e " \u2022 \e[0;36m${*}\e[m"; }
function echo_point() { echo "=> $* =" >> $LOG; echo -e "   \e[0;32m${*}\e[m"; }
function echo_ok() { echo "=> $* =" >> $LOG; echo -e "   \e[1;32m${*}\e[m"; }
function echo_ko() { echo "=> $* =" >> $LOG; echo -e "   \e[1;31m${*}\e[m"; }


# variables maintenance
site=$HOME"/"$(cat infos.txt |  grep wget_site | awk  -F ":" '{ print $2 }')
echo $site
rep=$HOME"/"$(cat infos.txt | grep local_location | awk -F ":" '{ print $2}' ) 
echo $rep

log=${rep}+"/logs/init.log"
infos="infos.txt"

mkdir -p ${rep}/logs
LOG=${rep}/logs/maintenance.log

# variables init
log_init=${rep}/logs/init.log

function sauvegarde() {
    site=$(cat $rep)/sites/site_sauvegarde
    return $site
} #site

function site() {
    site=$(cat ${rep}/sites/site_init)
    #echo "site : "$site
    return $site
} #site

function apt_update() { apt-get update >> $LOG 2>&1; }
function apt_upgrade() { apt-get full-upgrade -y >> $LOG 2>&1; }
function apt_install() { 
    echo_step "Installing $1"
    apt-get install $* -qy >> $LOG 2>&1 
    echo_ok "OK"
} #apt_install

function wget_file() { 
    echo_step "Downloading from $site"/"$1 to $rep"/"$1"
    echo "wget_file $*" >> $LOG; wget -nc $* >> $LOG 2&>1
    echo_ok "OK"
} #wget_file

function dpkg_file() {
    echo_step "Installing ${file} ..."
    #sudo dpkg -i $file >> $LOG 2>&1
    sudo dpkg -i $file >> $LOG 2>> $LOG
    case $? in
	    0)
    echo_ok -e "    OK"
    ;;
            *)
    echo_ko -e "    KO"
    ;;
    esac
} #dpkg_file

function f_packages() {
    case $1 in
basic)
    array=( "vim" "sudo" "pass" "gpg" "sshpass" "mcrypt" )
    ;;
admin)
    array=( "net-tools" "x2goclient" "nmap" "whois" "meld")
    ;;
bureau)
    array=( "i3" "redshift" "thunar"\
	    "xpdf" "baobab" )
    ;;
network)
    array=( "rsync" "tcpdump" "net-tools" "sshpass" "nmap" "tlslookup" )
    ;;
client)
    array=( "openssh-server" "x2goserver" "x2goserver-session" )
    ;;
dev)
    array=( "git" "python3" "python3-pip" "virtualenv" "virtualenvwrapper" )
    ;;
serveur)
    array=( "unattended-upgrades" "fail2ban" )
    ;;
dolibarr)
    array=( "apache2" "fail2ban" "php-intl" "mariadb-server" "git" "vim" )
    ;;
graphic)
    array=( "kile" "dia" "gimp" )
    ;;
clef_admin)
    array=$(clefs_admin( "carbone_rsa.pub" ))
    ;;
    esac
    use_array "${array[@]}"
    if [ $2 ]
        then
        for i in "${array[@]}"
        	do
        apt_install $i
        	done
    fi
} #f_packages

function clefs_admin () {
    array=( "authorized_keys" )
    use_array "${array[@]}"
} #clef_admin

use_array () { 
	 for idx in "$@"; 
	 do echo "$idx" 
	 done 
 } 

create_array () { 
	local array=("a" "b" "c") use_array "${array[@]}" 
}

function update() {
    echo_part "Update system"
    
    apt_update
    echo_step "Update repository"
    
    apt_upgrade
    echo_step "Upgrade packages"    
} #update

function admin() {
    echo_step "Mise à jour et installation utilitaires admin"
    f_packages admin 1
} #admin

function todo() {
    echo "x2goclient"
    echo "x2goserver"
    echo "openssh-server"
} #todo

function basic() {
    echo_part "Mise à jour et installation utilitaires de base"
    f_packages basic 1
} #basics

function client() {
    echo_part "MàJ & install poste client"
    f_packages client 1
} #client

function dev() {
	bashrc="pizzacoca-dev"
    echo_part "Mise à jour et installation outils dev"
    f_packages dev 1
    echo_step "Edition de bashrc :\e[m"
    echo_point "ajout de PS1 dans .bashrc"
	injection_bashrc $bashrc
} #dev

function graphics() {
    echo_part "Installation applis graphiques disponibles dans les dépots officiels"
    f_packages graphic 1
    file="XnViewMP-linux-x64.deb"
    cd /opt 
    wget_file https://download.xnview.com/${file}
    dpkg_file $file 
    
    file="blender-2.92.0-linux64.tar.xz"
    wget_file https://download.xnview.com/${file}

} # graphics

function sshadmin() {
	site=$(site)
    echo_part "Configuration poste client"
    echo_step "Ajout de client.config"
    wget_file ${site}/pub/client.config 
    mv client.config $HOME/.ssh/client.config

    echo "Include $HOME/.ssh/client.config" >> $HOME/.ssh/config

    echo_part "Ajout des clefs ssh d'administration"
    echo $site
    echo $rep
    admin=$(ls $rep/admin)  #( "authorized_keys" )
    for i in "${admin[@]}"
    	do
	echo "${site}/$i"
    wget_file ${site}/pub/admin/$i 
    cat $i >> $HOME/.ssh/authorized_keys
    rm $i
    echo_ok "$i ajoutée"
    	done
} #clientsshadmin
   
function sshGen() {
    echo_step "\e[36mGénération de la clef ssh $1\e[m"
    #echo_point -e "nom de la clef : "$HOME"/.ssh/"$HOSTNAME"_rsa"
    ssh-keygen -t rsa -b 4096 -f $1
} #sshGen

function sshclient() {
    clef=$HOME"/.ssh/"$HOSTNAME"_rsa"
    sshGen $clef
} #clientssh

function virtu() {
    processeurs=$(grep -E -c "vmx|svm" /proc/cpuinfo)
    echo_point "\e[1;31m$processeurs \e[0;36mprocesseurs compatibles et disponibles\e[m"
} #verif_virtu

function memo() {
    echo -e " Pour ajouter le GIT_SSH_COMMAND taper :"
    variable=$(cat /etc/hostname)
    nomclef=$(ls $HOME/.ssh/ | grep $variable | grep -v "pub" | grep -v "no_rsa")
    echo "export GIT_SSH_COMMAND='ssh -i "$HOME"/.ssh/"$nomclef"' git clone" 
    
    echo -e " Créer une clef ssh"
    echo "ssh-keygen -t rsa -b 4096"

    echo -e " Ajouter la clef à ssh-agent"
    echo 'eval "$(ssh-agent -s)"'
    echo "ssh-add $HOME/.ssh/"$nomclef
    
    echo -e " Ajouter le sudoer"
    echo -e "   sudo usermod -a -G sudo username"
    
    echo -e " Memo crontab"
    echo -e "    * * * * /usr/bin/autossh.sh"
} #fonctions a ajouter

function espace() {
	echo "./checkspace"
} #space

function getInfos() {
	cat infos.txt | grep $1 | cut -d: -f2
} #getInfos

function motdepasse() {
    read -r -s mdp
    echo $mdp
} #motdepasse

function verif_mdp(){
    echo -n "Votre mot de passe : "
    mdp=$(motdepasse)
    #export SSHPASS=$mdp
    echo_part $mdp
} #verif_mdp

function askYesNo() {
        QUESTION=$1
        DEFAULT=$2
        if [ "$DEFAULT" = true ]; then                    # Valeur par défaut définie en paramètre
                OPTIONS="[O/n]"
                DEFAULT="o"
            else
                OPTIONS="[o/N]"
                DEFAULT="n"
        fi
        read -p "$QUESTION $OPTIONS" INPUT 
        INPUT=${INPUT:-${DEFAULT}}                        # Si $INPUT vide => remplace par $DEFAULT
        if [[ "$INPUT" =~ ^[yYoO]$ ]]; then               # True si y,Y,O ou o
            return 1
	    #ANSWER=true
        else                                              # Faux pour le reste
            return 0
	    #ANSWER=false
        fi
} #askYesNo

function mkdir_action() {
    echo_point "création $1" 
    mkdir -p $1
} #mkdir_action


check_config() {
    if [ $rep ]
    then
	    echo_ko "$rep existant ( \$rep )"
    fi
    if [ $rep ]
    then
	    echo_ko "$rep existant ( \$rep )"
	    return 2
    else
    mkdir -p $rep/logs
    touch $log_init
    mkdir_action $rep
    mkdir_action $rep/logs/
    mkdir_action $rep/keys/
    mkdir_action $rep/sites/
    mkdir_action $HOME/bin/
    mkdir_action $HOME/.config
    fi
} #check_config

function init() {
	echo 
	echo "site de récupération ? ( defaut : $site )"
    read -r site_init
    echo_step "téléchargement depuis $site vers $site_init "
    cd $rep
    echo $rep
    mkdir_action $rep
    mkdir_action $rep/logs/
    mkdir_action $rep/keys/
    mkdir_action $rep/sites/
    #mkdir_action $HOME/bin/
    echo $rep
    wget_file ${site_init}infos.txt -O ${rep}/infos.txt
    admin=( "authorized_keys" )
    for i in "${admin[@]}"
    	do
    wget_file ${site_init}$i -O $rep/keys/$i
        done 
    wget_file ${site_init}maintenance.sh -O ${rep}maintenance.sh
    cd $rep
    chmod +x ${rep}maintenance.sh
} #install

# ----------------------------------------------------------- .bashrc 

function injection_bashrc() {
	case $1 in
		pizzacoca-alias) array=("# $1------"\
			"if [ -f ~/.config/baionet/bash_aliases ]; then"\
    		". ~/.config/baionet/bash_aliases"\
			"fi"\
			);;
		pizzacoca-history) array=("# $1 ------"\
			"export PROMPT_COMMAND='history -a'"\
			"") ;;
		pizzacoca-dev) array=("# $1 ------"\
			"export PS1='\[\033[0;37m\]\h:\[\033[0;33m\]\W\[\033[0m\]\[\033[1;32m\]\$(__git_ps1)\[\033[0m\] \$ '"\
			"export EDITOR=vim"\
			"# enable core files (for help to debug)"\
			"ulimit -c 200000"\
			"")
			# on décommente des lignes déja existantes dans le ~/.bashrc
    		sed -i -e "s/#alias ll/alias ll/g" $HOME/.bashrc
    		sed -i -e "s/#force_color_prompt/force_color_prompt/g" $HOME/.bashrc
			;;
		pizzacoca-ip) array=("# $i ------"\
			"ip -4 -br -c=always  a | grep UP "\
			"ip -6 -br -c=always  a | grep UP"\
			"") ;;
		pizzacoca-gpg) array=("# $i ------"\
			"GPG_TTY=\$(tty)"\
			"export GPG_TTY"\
			"") ;;
		pizzacoca-divers) array=("# $i ------"\
			""\
			""\
			) ;;
		*) array=("# $i ------"\
			""\
			) ;;
	esac
	for i in "${array[@]}"
	do
		value=$i
		echo ${value} >> ~/.bashrc
	done
} # injection_bashrc
 
function bashrc() {
	echo_part "Modification de ~/.bashrc"
		bashrc_chapter=("pizzacoca-alias"  "pizzacoca-history" "pizzacoca-ip" "pizzacoca-gpg" )
	for i in "${bashrc_chapter[@]}"
	do
		value=$i
		check=$(cat ~/.bashrc | grep ${value} | awk '{ print $2 }')
		if  [ ${check} ] ;
   		then 
			echo_step "${value} est deja present"
		else 
			echo_step "${value} n'est pas présent, injection dans ~/.bashrc"
			injection_bashrc ${value}
		fi
	done
} #bashrc

# ----------------------------------------------------------- /.bashrc 
	

function help() {
    basics=$(f_packages basic)
    devs=$(f_packages dev)
    graphics=$(f_packages graphic)
    admin=$(f_packages admin)
    client=$(f_packages client)
    bureau=$(f_packages bureau) 
    network=$(f_packages network) 
    serveur=$(f_packages serveur) 
	dolibarr=$(f_packages dolibarr) 
    echo_part_menu "\nConfiguration script for Debian linux system"
    echo_part_menu "\e[1;31mGPLv3 \e[0;32mand above"
    echo_part_menu "Pizzacoca 2021"
    
   
    echo_part_menu "\nHelp (Memo does nothing on system)"

    echo_step_menu "virtu\e[m  : Vérification capacité virtualisation"
    echo_step_menu "memo\e[m  : memo commandes"

    echo_part_menu "\ninitialisation"
    echo_step_menu "init  :\e[m initialisation du répertoire de config avec init.sh"

    echo_part_menu "\nHelp (Maintenance)"

    echo_step_menu "espace     :\e[m récupération d'espace"
    echo_step_menu "sshclient :\e[m création d'une clef ssh sur le poste client"
    echo_step_menu "sshadmin  :\e[m ajout de la clef ssh admin sur le poste client"
    echo_step_menu "savessh     :\e[m sauvegarde des clefs ssh perso"
    echo_step_menu "bashrc     :\e[m Tuning du .bashrc, ajout d'aliases"
 

    echo_part_menu "\nroot (mises à jour et installations)"
    
    echo_step_menu "update    :\e[m apt-get update and full-upgrade -y"
    echo_step_menu "basic     :\e[m minimal system conf : \e[33m" ${basics[*]// /|}
    echo_step_menu "client    :\e[m outils client administré : \e[33m" ${client[*]// /|}
    echo_step_menu "bureau     :\e[m installation bureau : \e[33m" ${bureau[*]// /|}
    echo_step_menu "network     :\e[m outils réseau : \e[33m" ${network[*]// /|}
    echo_step_menu "graphics  :\e[m outils graphiques : \e[33m" ${graphics[*]// /|} blender XnView-MP
    echo_step_menu "dev :\e[m   : outils dev : \e[33m" ${devs[*]// /|}
    echo_step_menu "admin     :\e[m : outils admin : \e[33m" ${admin[*]// /|}
    echo_step_menu "serveur     :\e[m : utils serveur : \e[33m" ${admin[*]// /|}
    echo_step_menu "dolibarr    :\e[m : outils dolibarr : \e[33m" ${admin[*]// /|}

    echo_part_menu "\nroot (config rapide)"
    
    echo_step_menu "i0 =\e[m update + basic"
    echo_step_menu "i1 =\e[m i0 + client"

    echo_part_menu "\nLog : \e[m$LOG"
    echo_part_menu "\nLog init : \e[m$log_init"
    exit 0
} #help


# appel des fonctions
function i0() { exec $0 update basic $basics; }
function i1() { exec $0 update basic client; }
#log_f

[ $# -eq 0 ] && help
	echo -e "Configure : $*" >> $LOG
	echo -e "$(date)" >> $LOG
	
	for RUN in $*
	do
	  echo -e "\n[ $RUN ]" >> $LOG
	  $(declare -f -F $RUN)
	done
	
	echo -e "\nDone." >> ${LOG}
	echo -e "Configure : $*" >> $LOG
	echo -e "$(date)" >> $LOG
	
	[ $# -eq 0 ] && help
	
	echo -e "Configure : $*" >> $log_init
				#""\
	echo -e "$(date)" >> $log_init

