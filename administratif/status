====== Statuts 2022 ======


====== Article 1 – Titre de l’Association ======

Il est fondé entre les adhérents aux présents statuts une Association régie par la loi du premier juillet 1901 et le décret du 16 août 1901, ayant pour titre : **Baionet**

====== Article 2 – But de l’Association ======

L’association a pour but : 

La promotion, l’utilisation et le développement du réseau Internet dans le respect de son éthique en favorisant en particulier les utilisations à des fins de recherche ou d’éducation sans volonté commerciale ;

La promotion et l'utilisation des logiciels libres.

De favoriser la compréhension du réseau Internet et de ses enjeux par le public. Ceci dans une optique de réappropriation horizontale et décentralisée du réseau et des outils en relation ;

De défendre un réseau Internet aidant à la liberté d'expression, entre autre par le rtespect du principe de neutralité du net et de l'acentrisme du réseau ;

De défendre le réseau Internet comme un réseau ouvert, acentré, neutre et libre.
====== Article 3 – Siège social ======

Le siège social est fixé à cette adresse :

> **Cocoba 1317** 
> **Route départementale 817**  
> **64270 Puyoô**
> **France**

Il pourra être transféré sur décision du Bureau.

====== Article 4 – Moyens d'action ======

Les moyens d'action de l'association sont notamment :

De fournir à ses adhérents la possibilité d'accéder à Internet, c'est-à-dire au service qui offre la capacité de transmettre et de recevoir des données, en utilisant le protocole de communication IP, depuis toutes les extrémités désignées par une adresse Internet publique, de l'ensemble mondial de réseaux interconnectés constituant Internet ; 

La mise à disposition de ses adhérents de services de communication au public en ligne tels que le mail, la messagerie instantanée, le transfert de fichiers, l'hébergement de contenus et plus généralement toutes applications fonctionnant sur Internet ;

Les publications sur tous supports, les actions de formation et d'éducation populaire en rapport avec l'objet de l'association ;

La défense des positions de l'association auprès de toutes instances, locales, nationales voire internationales, lors de tous travaux et débats, y compris en justice ; 

Toutes actions permettant de faire connaître, défendre, promouvoir ou aider à la réalisation de l'objet de l'association ; 

La vente permanente ou occasionnelle de tous produits ou services entrant dans le cadre de son objet ou susceptible de contribuer à sa réalisation.

====== Article 5 – Durée de l'association ======

La durée de l'association est **illimitée.**

====== Article 6 – Composition de l'association ======

L'Association se compose de personnes physiques et de personnes morales légalement constituées.
Dans le cas d'une personne morale, elle doit être représentées par une ou plusieurs personnes physiques nécessairement impliquées dans l'activité de ladite personne morale.

L'Association se compose de : 

  * Membres d'honneur ;
  * Membres bienfaiteurs ; 
  * Membres adhérents.

**Membres d'honneur**

Sont membres d'honneur ceux qui ont été désignés comme tels par une Assemblée Générale Ordinaire ou Extraordinaire sur proposition du bureau en raison des services éminents qu'ils ont rendu à l'Association.   
  * Ils sont dispensés de cotisations. 
  * Ils ont le droit de participer à l'Assemblée Générale sans voix délibérative.
  * Cette catégorie de membre ne donne aucun droit supplémentaire.

**Membres bienfaiteurs**

Sont membres bienfaiteurs ceux qui versent la cotisation annuelle telle que fixée chaque année pour cette catégorie de membres par le Bureau. 
  * Ils ont le droit de participer à l'Assemblée Générale avec voix délibérative. 
  * Cette catégorie ne donne aucun droit supplémentaire.

**Membres adhérents**

Sont membres adhérents ceux qui versent la cotisation normale telle que fixée par le Bureau. 
  * Ils ont le doit de participer à l'Assemblée Générale avec voix délibérative. 
  * Cette catégorie de membre de donne aucun droit supplémentaire.
  * D'autre part, les membres adhérents peuvent être soit membres actifs, soit membres passifs. 
    * Sont dits "membres actifs" ceux qui assistent à l'Assemblée Générale annuelle, 
    * ou sont membres du Bureau, 
    * ou ont par un moyen ou un autre participé à la vie de l'Association autrement que par le paiement de la cotisation.

====== Article 7 – Admission et adhésion ======

Pour faire partie de l'association, il faut :

  * Adhérer aux présents statuts, 
  * S'acquitter de la cotisation dont le montant est fixé par le Bureau qui statue lors de chacune de ses réunions sur les demandes en cours avec avis.


====== Article 8 – Perte de la qualité de membre ======

La qualité de membre se perd par : 

  * La démission de la personne physique ou morale, par lettre adressée au Bureau de l'Association. Dans le cas d'une personne morale, la démission devra être soumise par une ou plusieurs personnes physiques nécessairement impliquées de la dite personne morale ; 
  * Le décès de la personne physique ou la dissolution de la personne morale ; 
  * L'exclusion ou radiation pour non-paiement de la cotisation ou pour infraction aux statuts ou pour motif portant préjudice aux intérêts moraux ou matériels de l'association, ou pour tout autre motif grave. Elle est prononcée par le Bureau après que l'intéressé(e) ait été invité(e) à se présenter devant lui pour fournir des explications.


Le règlement intérieur pourra préciser quels sont les motifs graves.

======Article 9 – Responsabilité des membres======

  * Aucun des membres de l'association n'est personnellement responsable des engagements contractés par elle. 
  * Seul le patrimoine de l'association répond de ses engagements. 
  * En matière de gestion, la responsabilité incombe, sous réserve d'appréciation souveraine des tribunaux, à l'Assemblée Générale et aux membres du Bureau.

====== Article 10 – Composition du Bureau ======

L'Association est dirigée par un Bureau composé d'au minimum deux membres. 

Le Bureau est investi des pouvoirs nécessaires au fonctionnement de l'Association. Les décisions sont prises à la majorité des membres du Bureau.

Le renouvellement du Bureau est soumis à l'approbation de l'Assemblée Générale, sur proposition du Bureau. Toutefois, si celui-ci n'est pas en mesure de soumettre une proposition à l'Assemblée Générale, celle-ci élit directement les membres du Bureau. Les membres sont ré-éligibles.

Chaque membre du Bureau peut représenter l'Association dans tout acte de la vie civile ou pour remplir toute formalité de déclaration et de publication prescrite par la législation ou tout acte administratif nécessaire au fonctionnement de l'Association.

Chaque membre du Bureau a qualité pour présenter toute réclamation après de toutes administrations, notamment en matière fiscale, et pour s'exprimer au nom de l'Association vis-à-vis de ses interlocuteurs ou des médias ou d'ester en justice au nom de l'Association tant en demande qu'en défense.

Vis-à-vis des organismes bancaires ou postaux, chaque membre du Bureau à pouvoir, individuellement, de signer tout moyen de paiement (chèques, virements, etc) et tout document ayant trait au service postal fourni à l'Association.

====== Article 11 – Attributions du Bureau ======

Le Bureau est investi des pouvoirs les plus étendus dans la limite de l'objet de l'Association et dans le cadre des résolutions adoptées par l'Assemblée Générale. Il peut autoriser tout acte ou opération qui ne sont pas statutairement de la compétence de l'Assemblée Générale Ordinaire ou Extraordinaire.

Le Bureau établit l'ordre du jour des Assemblées Générales, prépare les bilans qui y seront présentés et assure l'exécution des décisions de ces Assemblées. Il établit des propositions de modifications des statuts qui seront présentées à l'assemblée Générale Extraordinaire.

Le Bureau autorise toutes acquisitions, aliénations, ou locations immobilières ainsi que les contrats à intervenir entre l'Association et les Collectivités ou Organismes publics qui lui apportent une aide financière. Ces autorisations sont faites uniquement à l'unanimité des membres du Bureau présents lors d'une réunion.

Le Bureau établit le budget de l'Association et il fixe le montant des cotisations. Il ordonnance les dépenses de l'Association.

Le Bureau assure le bon fonctionnement de l'Association sous le contrôle de l'Assemblée Générale ordinaire dont il prépare les réunions.

Le Bureau peut déléguer tel ou tel de ses pouvoirs pour une durée indéterminée, à un ou plusieurs de ses membres, en conformité avec l'éventuel règlement intérieur.

====== Article 12 – Les Assemblées Générales Ordinaires ======

L'assemblée générale ordinaire est exclusivement ouverte aux membres du Bureau ainsi qu'à tous les membres à jour de leur cotisation. 


Les adhérents en faisant la demande par écrit, pourront se faire représenter par un membre de leur famille ou par un autre membre de l'Association, ou voter par correspondance (sous forme numérique sur le serveur de l'Association, ou via simple courrier).


La procuration n'est pas transmissible. Le Bureau statue sur la validité des procurations lors des votes.

En cas de partage, la voix de chacun des membres du Bureau est prépondérante.

L'Assemblée Générale a lieu au moins une fois par an. L'Assemblée Générale Ordinaire peut également être convoquée à tout moment à la demande de la majorité des membres du Bureau. Elle est convoquée par le Bureau au moins quinze jours avant la date fixée.

Elle entend les rapports sur :
  * La gestion du Bureau
  * Les activités de l'Association 
  * La situation morale de l'Association
  * La situation financière de l'Association

Enfin, l'assemblée générale délibère sur les orientations à venir.

Les décisions sont prises à la majorité absolue des suffrages exprimés par les membres présents ou représentés.

Une assemblée peut se tenir sous forme électronique conformément aux dispositions prévues par l'éventuel règlement intérieur.

====== Article 13 – Les Assemblées Générales Extra-ordinaires. ======

L'Assemblée Générale Extra-ordinaire est exclusivement ouverte aux membres du Bureau ainsi qu'à tous les membres depuis plus d'un an et à jour de leur cotisations. les membres peuvent se faire représenter par un membre de leur famille ou par un autre membre de l'Association. la procuration n'est pas transmissible. Le Bureau statue sur la validité des procurations lors des votes.

En cas de partages, la voix de chacun des membres du Bureau est prépondérante. 

Elle se réunit à la demande de la majorité des membres du Bureau.

L'Assemblée Générale Extraordinaire se prononce sur les modifications à apporter aux statuts et sur la dissolution de l'Association. L'Assemblée Générale Extra-ordinaire a également la possibilité de prendre toutes les décisions prévues pour l'Assemblée Générale Ordinaire, et ce dans les mêmes circonstances, c'est-à-dire sans minimum de représentation des membres, et à la majorité absolue des suffrages exprimés. 

L'Assemblée Générale Extraordinaire ne peut se prononcer valablement que si les deux-tiers des membres actifs de l'Association sont présents ou représentés. Si plus de la moitié des membres à jour de leurs cotisations et membres depuis plus d'un an étaient présents, alors les décisions de l'Assemblée dont le degré d'urgence le permettrait seraient soumises à l'approbation, par vote électronique, de l'ensemble des adhérents. Ce vote, à approbation implicite, se ferait à la majorité absolue des membres de l'Association.

Les décisions sont prises à majorité des deux-tiers des suffrages exprimés par les membres présents ou représentés.

Si le quorum des deux-tiers des membres actifs n'était pas atteint, l'Assemblée serait, de facto, une Assemblée Générale Ordinaire, et statuerait sur les points de l'ordre du jour qui le permettent.

Si le quorum n'est pas atteint, l'Assemblée Générale Extraordinaire est convoquée à nouveau, sous trente jours. Elle peut alors délibérer quel que soit le nombre de présents et représentés.

====== Article 14 – Rémunération ======

Les fonctions des membres du Bureau sont bénévoles, seuls les frais et débours occasionnés pour l'accomplissement du mandat d'administrateur peuvent éventuellement être remboursés, au vu des pièces justificatives, après accord du trésorier et si les comptes de l'association permettent l'ordonnance du remboursement.

Le rapport financier présenté à l'Assemblée Générale ordinaire doit faire mention des remboursements de frais de mission, de déplacement ou de représentation payés à des membres du Bureau.
====== Article 15 – Règlement intérieur ======


Un règlement intérieur est établi par le Bureau qui le fait approuver par l’Assemblée Générale Ordinaire. 

Il s'impose à tous les membres, au même titre que les statuts. 

Il précise les règles de fonctionnement et d'organisation de l'Association, ainsi que tous les éléments jugés utiles pour le bon fonctionnement de l'Association qui ne sont pas prévus dans les présents statuts.

====== Article 16 – Ressources de l'Association ======

Les ressources de l'Association se composent : 

  * Des cotisations ;
  * Des rétributions des services rendus ou des prestations fournies par l'Association
  * Des subventions de l'État, des collectivités territoriales et des établissements publics ;
  * Du produit des manifestations qu'elle organise ;
  * Des intérêts et redevances des biens et valeurs qu'elle peut posséder ; 
  * De dons manuels ;
  * De toutes autres ressources autorisées par la loi, notamment, le recours en cas de nécessité, à un ou plusieurs emprunts bancaires ou privés.

Il est tenu une comptabilité faisant apparaître annuellement un compte de résultat, un bilan et une annexe, conformément à la réglementation en vigueur.

====== Article 17 – Dissolution ======

En cas de dissolution, l'Assemblée Générale Extraordinaire désigne un ou plusieurs liquidateurs qui seront chargés de la liquidation des biens de l'Association et dont elle détermine les pouvoirs.

Les membres de l'Association ne peuvent se voir attribuer, en dehors de la reprise de leurs apports financiers, mobiliers ou immobiliers, une part quelconque des biens de l'Association.

L'actif net subsistant sera attribué obligatoirement à une ou plusieurs associations poursuivant des buts similaires et qui seront désignés par l'Assemblée Générale Extraordinaire. 

====== Dépôt ======

[[https://www.journal-officiel.gouv.fr/associations/detail-annonce/associations_b/20170045/950|Permalink du dépôt au Journal Officiel]]  

Date de déclaration : le 02/11/2017 

Lieu de déclaration : Sous-préfecture Bayonne 

Date de publication : 11/11/2017

N° RNA : W641008381

N° de parution : 20170045

N° d’annonce : 950


