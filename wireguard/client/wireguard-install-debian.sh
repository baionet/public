# !/bin/bash
# Script tested under debian bullseye
# no garantee !
# from pizzacoca 
# GPLv3


########## Variables #############
##### C'est ici qu'il faut indiquer l'ip publique souhaitée
plage="80.67.186.77/32"
plage6="2001:913:5101::77/56"
#####

plage_test="80.67.186.99/32"
machine=$(cat /etc/hostname)
endpoint="80.67.186.254:51820"
server_pubkey="pmcbNukQLOFSdImt2Tzy5gG4NLDOxHGI4MYGugBKWAE="

path_conf="/etc/wireguard/"
path_archive="/etc/wireguard/archives/"
num_conf=$(ls ${path_conf} | wc -l)

path_interface="/etc/network/interfaces.d/"

########## Gestion de l'affichage couleur #############
function echo_part_menu() { echo -e "\e[0;32m${*}\e[m"; }
function echo_step_menu() { echo -e " \u2022 \e[0;36m${*}\e[m"; }
function echo_ok_menu() { echo -e "   \e[1;32m${*}\e[m"; }
function echo_ko_menu() { echo -e "   \e[1;31m${*}\e[m"; }


echo_part_menu "installation et génération de conf wireguard IPv4 only"
echo_step_menu "mise à jour ..."
apt update && apt upgrade

echo_step_menu "installation des paquets"
apt install wireguard wireguard-dkms

# apt install resolvconf


# On vérifie l'existence d'une install précédente
if [ -d "${path_archive}" ];then
	echo_step_menu "${path_archive} existe déja, passage à l'étape suivante";
else
	echo_step_menu "Création du répertoire d'archivage ${path_archive}"
	mkdir $path_archive
fi

# On vérifie le nombre de configurations déja existantes
num_interface=$(ls ${path_interface} | wc -l)
num_conf=$(ls ${path_conf} | grep .conf | wc -l)

# petite alerte en cas de différence
if [ ${num_interface} != ${num_conf} ];then
	echo_ko_menu "${num_interface} interfaces supplémentaires pour ${num_conf} wireguard !"
	echo_step_menu  "Continuer ? [n/o]:"
	read continuer	
	if [ $continuer = "o" ]
	then
		echo_part_menu "On continue"
		pass
	else
		exit 45
	fi

else
	echo_ok_menu "${num_interface} interfaces supplémentaires pour ${num_conf} wireguard on peut continuer."
fi 


# génération des clefs 
echo_step_menu "Déplacement dans le répertoire ${path_archive}"
cd $path_archive
#/etc/wireguard

pre="zw9ZGlYuZRYCQuM4g96I9shpQ3OF3Ma4xupOFaItx5c="
priv="YFagaTJHcwhP4TSaTV32zZ7Z0EMeNNbjnWoflmwEIEQ="
pub="byvV0inCLWIcCW+cyrwnYk7b/gLA9eqI0xS3ZB8wFUM="

# vérification de l'adresse wireguard souhaitée
if [ ${plage} != "80.67.186.77/32"]
# On demande si c'est une installation de test
echo_step_menu  "l'adresse souhaitée est ${$plage} [n/o]:"
	read continuer	
	if [ $continuer = "o" ]
	then
		plage=${plage_test}
		echo_part_menu "addresse du tunnel définie à ${plage}"
		echo_part_menu "On continue"
		pass
	else
		echo_part_menu "addresse du tunnel définie à ${plage}"
		sleep 1
		echo_step_menu "Génération des clefs"
		wg genkey | tee ${path_archive}private-wg${num_conf}.key | wg pubkey > ${path_archive}public-wg${num_conf}.key
		
		echo_step_menu "Création du secret partagé"
		wg genpsk > ${path_archive}preshared-wg${num_conf}.key
		
		echo_step_menu "Changement des droits sur les fichiers"
		chmod 600 ${path_archive}private-wg${num_conf}.key
		chmod 600 ${path_archive}preshared-wg${num_conf}.key
		
		pre=$(cat ${path_archive}preshared-wg${num_conf}.key)
		priv=$(cat ${path_archive}private-wg${num_conf}.key)
		pub=$(cat ${path_archive}public-wg${num_conf}.key)
	fi

# On affiche la configuration de l'interface à installer
	interface='
# Interface au démarrage\n
auto wg'${num_interface}'\n
\n'

echo "######## fichier ${path_interface}wg${num_interface} ####################"
	echo -e $interface
echo "######## fin du fichier ###############################"

# On demande si on veut créer l'interface
	echo_step_menu  "Créer cette nouvelle interface  ${path_interface}wg${num_interface} ? [n/o]:"
	read save_config	
	if [ $save_config = "o" ]
	then
			echo -e $interface > ${path_interface}wg${num_interface}
			echo_part_menu "${path_interface}wg${num_interface} créée"
			echo -e "liste des interfaces existantes : \n"
			ls ${path_interface}
	else
			echo_ko_menu "création de la nouvelle interface annulée"
	fi
echo "Construction de la configuration wireguard ..."
sleep 1

# On affiche la configuration wireguard proposée
	conf='
# définition du service wireguard\n
\n
[Interface]\n
	Address = '${plage}'\n
	# contenu de la clef privée\n
	PrivateKey = '${priv}'\n
\n	
	# "MSS fucking clamping"
	PostUp = ip route change default dev wg'${num_interface}' advmss 1360 table 51820
\n
	# On impose le port écouté\n
	ListenPort = 44451\n
\n
[Peer]\n 
	# clef publique du serveur\n
	PublicKey = '${server_pubkey}'\n
\n
	# secret partagé\n
	PresharedKey = '${pre}'\n
\n	
	AllowedIPs = 0.0.0.0/0, ::/0\n
\n	
	#Adresse_publique_serveur:numero_de_port\n
	Endpoint = '${endpoint}'\n
\n
	# on veut une persistence de la connexion\n
	PersistentKeepalive =25\n'

echo "######## Fichier ${path_interface}wg${num_interface} ###############################"
echo -e ${conf}
echo "######## Fin du fichier ###############################"

# On demande si la configuration est à enregistrer
	echo_step_menu  "Enregistrer la configuration ${path_conf}wg${num_interface}.conf ? [n/o]:"
	read save_conf
	
	if [ $save_conf = "o" ]
	then
			echo -e ${conf} > ${path_conf}wg${num_interface}.conf
			echo_part_menu "${path_conf}wg${num_interface}.conf enregistré"
			echo -e "liste des configurations existantes : \n"
			ls ${path_conf}
	else
			echo_ko_menu "Création de la configuration annulée"
	fi
echo "Construction de la configuration coté serveur ..."
sleep 1

echo "Voulez-vous activer la géolocalisation ? o/n"
read blague
echo "merci d'avoir accepté nos conditions d'utilisation"
sleep 1

# On affiche la configuration à mettre coté serveur
	echo_step_menu "\nConfiguration coté serveur à communiquer à baionetek@framalistes.org :\n"
	conf_server='
# wg'${num_interface}' sur '${machine}' IPv4 & IPv6\n
[Peer]\n
PublicKey ='${pub}'\n
PresharedKey ='${pre}'\n
AllowedIPs = '${plage}', '${plage6}'\n '

echo_ok_menu "###### Infos à transmettre #################################"
	echo -e ${conf_server}
echo_ok_menu "###### Fin des infos à transmettre #################################"

# On demande si la configuration est à enregistrer
echo_step_menu  "Archiver ${path_archive}wg${num_interface}-${machine}.baionet ? [n/o]: "
read save_conf

if [ $save_conf = "o" ]
then
	echo -e ${conf_server} > ${path_archive}wg${num_interface}-${machine}.baionet
	chmod 600 ${path_archive}wg${num_interface}-${machine}.baionet
	echo_part_menu "${path_archive}wg${num_interface}-${machine}.baionet enregistré"
	echo_step_menu "liste des configurations existantes : \n"
	ls ${path_conf} | grep *.conf
	echo_part_menu "pour lancer le tunnel :"
	echo "wg-quick up wg${num_interface}" 
	echo_part_menu "Pour lancer le tunnel a chaque demarrage"
	echo "systemctl enable wg-quick@wg${num_interface}" 

	echo_part_menu "---- en cas de dysfonctionnement ----"
	echo_part_menu "assigner l'adresse à la nouvelle interface :"
	echo "ip address add ${plage} dev wg${num_interface}"
	echo "ip -6 address add ${plage6} dev wg${num_interface}"
	echo_part_menu "assigner une nouvelle route :"
	echo "ip route add 10.0.0.1 dev wg${num_interface}"
 	echo "post-up ip -6 route add   fd64:ba10:cafe:c0ca::1 dev wg${num_interface}"

	echo_part_menu "\n\n${path_archive}wg${num_interface}-${machine}.baionet est à communiquer à baionetek@framalistes.org"

else
		echo_ko_menu "Sauvegarde annulée"
fi

