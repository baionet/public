# !/bin/sh
# Script testé sous debian bullseye
# from pizzacoca 
# GPLv3

echo "installation et génération de conf wireguard IPv4 only"

machine=$(cat /etc/hostname)
plage="80.67.186.99/32"
endpoint="80.67.186.254:51820"
server_pubkey="pmcbNukQLOFSdImt2Tzy5gG4NLDOxHGI4MYGugBKWAE="
path_conf="/etc/wireguard/"
num_conf=$(ls ${path_conf} | wc -l)

path_interface="/etc/network/interface.d/"
num_interface=$(ls ${path_interface} | wc -l)
echo "mise à jour ..."
apt update && apt upgrade

echo "installation des paquets"
apt install wireguard wireguard-dkms

# apt install resolvconf

pre="zw9ZGlYuZRYCQuM4g96I9shpQ3OF3Ma4xupOFaItx5c="
priv="YFagaTJHcwhP4TSaTV32zZ7Z0EMeNNbjnWoflmwEIEQ="
pub="byvV0inCLWIcCW+cyrwnYk7b/gLA9eqI0xS3ZB8wFUM="

# On affiche la configuration de l'interface
	echo -e "\nConfiguration de l'interface etc/network/wg${num_interface}\n"
	interface='

[Interface]\n
	# contenu de la clef privée\n
	PrivateKey = '${priv}'\n
\n
	# adresse assignée par le serveur/fournisseur\n
	Address = '${plage}'\n
	# DNS proposés par le serveur\n
	DNS = 80.67.169.12, 80.67.169.40\n
	# On impose le port écouté\n
	ListenPort = 44451\n
\n
[Peer]
	# clef publique du serveur\n
	PublicKey = '${server_pubkey}'\n
\n
	# secret partagé\n
	PresharedKey = '${pre}'\n
\n
	AllowedIPs = 0.0.0.0/0\n
\n
	#Adresse_publique_serveur:numero_de_port\n
	Endpoint = '${endpoint}'\n
\n
	# on veut une persistence de la connexion\n
	PersistentKeepalive = 25\n'

	echo -e $interface

# On demande si on veut créer l'interface
	echo -n  'Créer cette nouvelle interface wg'${num_interface}' ? [n/o]: '
	read save_config
	
	if [ $save_config = "o" ]
	then
			echo -e $interface > ${path_interface}wg${num_interface}
			echo -e "liste des interfaces existantes : \n"
			ls ${path_interface}
	else
			echo "création de la nouvelle interface annulée"
	fi

# On affiche la configuration wireguard proposée
	echo -e "\nConfiguration de ${path_conf}wg${num_conf}.conf\n"
	conf='

# définition du service wireguard\n
\n
[Interface]\n
	# contenu de la clef privée\n
	PrivateKey = '${priv}'\n
\n	
	# adresse ip définie dans /etc/interface/interface.d/wg'${num_interface}'\n
\n
	# On impose le port écouté\n
	ListenPort = 44451\n
\n
[Peer]\n
	# clef publique du serveur\n
	PublicKey = '${server_pubkey}'\n
\n
	# secret partagé\n
	PresharedKey = '${pre}'\n
\n	
	AllowedIPs = 0.0.0.0/0, ::/0\n
\n	
	#Adresse_publique_serveur:numero_de_port\n
	Endpoint = '${endpoint}'\n
\n
	# on veut une persistence de la connexion\n
	PersistentKeepalive = 25\n'

	echo -e ${conf}

# On demande si la configuration est à appliquer
	echo -n  'Enregistrer la configuration '${path_conf}'wg'${num_conf}'.conf ? [n/o]: '
	read save_conf
	
	if [ $save_conf = "o" ]
	then
			echo -e ${conf} > ${path_conf}wg${num_conf}.conf
			echo -e "liste des configurations existantes : \n"
			ls ${path_conf}
	else
			echo "configuration annulée"
	fi

# On affiche la configuration à mettre coté serveur
	echo -e "\nConfiguration coté serveur à communiquer à baionetek@framalistes.org :\n"
	conf_server='
# ${host} only IPv4\n
#[Peer]\n
#PublicKey ='${pub}'\n
#PresharedKey ='${pre}'\n
#AllowedIPs = '${plage}'\n '

	echo -e ${conf_server}

# On demande si la configuration est à appliquer
	echo -n  'Enregistrer '${path_conf}'wg'${machine}'.baionet ? [n/o]: '
	read save_conf
	
	if [ $save_conf = "o" ]
	then
			echo -e ${conf} > ${path_conf}${machine}-wg${num_conf}.conf
			echo -e "liste des configurations existantes : \n"
			ls ${path_conf}
	else
			echo "${path_conf}wg${machine}.baionet est à envoyer à baionetek@framalistes.org"
	fi

