# !/bin/bash
# Script tested under debian bullseye
# no garantee !
# from pizzacoca 
# GPLv3


########## Variables #############
##### C'est ici qu'il faut indiquer l'ip publique souhaitée
plage="80.67.186.90/32"
plage6="2001:913:5101::90/56"
#####

plage_test="80.67.186.99/32"
machine=$(cat /etc/hostname)
endpoint="80.67.186.254:51820"
server_pubkey="pmcbNukQLOFSdImt2Tzy5gG4NLDOxHGI4MYGugBKWAE="

path_conf="/etc/wireguard/"
path_archive="/etc/wireguard/archives/"

if [ ! -d ${path_archive} ];then
	echo "Creation du dossier ${path_archive} ";
	mkdir ${path_archive}
fi

num_conf=$(ls ${path_archive} | wc -l)

path_interface="/etc/network/interfaces.d/"

########## Gestion de l'affichage couleur #############
function echo_part_menu() { echo -e "\e[0;32m${*}\e[m"; }
function echo_step_menu() { echo -e "\e[0;36m${*}\e[m"; }
function echo_ok_menu() { echo -e "\e[1;32m${*}\e[m"; }
function echo_ko_menu() { echo -e "\e[1;31m${*}\e[m"; }


echo "generation de conf wireguard IPv4 only"

pre="zw9ZGlYuZRYCQuM4g96I9shpQ3OF3Ma4xupOFaItx5c="
priv="YFagaTJHcwhP4TSaTV32zZ7Z0EMeNNbjnWoflmwEIEQ="
pub="byvV0inCLWIcCW+cyrwnYk7b/gLA9eqI0xS3ZB8wFUM="

# vérification de l'adresse wireguard souhaitée
echo "Adresse souhaitee :"
sleep 0.5
echo_part_menu  "${plage}"
sleep 1

echo "Generation de la clef privee"
umask 077
sleep 0.5
wg genkey | tee ${path_archive}private-wg${num_conf}.key | wg pubkey > ${path_archive}public-wg${num_conf}.key
echo_part_menu "${path_archive}private-wg${num_conf}.key"
sleep 1
echo "Generation de la clef publique"
sleep 0.5
echo_part_menu "${path_archive}public-wg${num_conf}.key"
sleep 1
echo "Generation du secret partage"
sleep 0.5
wg genpsk > ${path_archive}preshared-wg${num_conf}.key
echo_part_menu "${path_archive}preshared-wg${num_conf}.key"
umask 0022
sleep 1

pre=$(cat ${path_archive}preshared-wg${num_conf}.key)
priv=$(cat ${path_archive}private-wg${num_conf}.key)
pub=$(cat ${path_archive}public-wg${num_conf}.key)


echo "Generation du fichier de configuration wireguard ..."
sleep 0.5

# configuration wireguard proposée
        conf='
# definition du service wireguard\n
\n
[Interface]\n
        Address = '${plage}'\n
        # contenu de la clef privee\n
        PrivateKey = '${priv}'\n
\n
        # "MSS clamping"
        #PostUp = ip route change default dev wg'${num_conf}' advmss 1360 table 51820
	MTU = 1280
\n
        # On impose le port ecoute\n
        ListenPort = 44451\n
\n
[Peer]\n
        # clef publique du serveur\n
        PublicKey = '${server_pubkey}'\n
\n
        # secret partagé\n

		        PresharedKey = '${pre}'\n
\n
        AllowedIPs = 0.0.0.0/0, ::/0\n
\n
        #Adresse_publique_serveur:numero_de_port\n
        Endpoint = '${endpoint}'\n
\n
        # on veut une persistence de la connexion\n
        PersistentKeepalive =25\n'

echo -e ${conf} > ${path_conf}wg${num_conf}.conf
echo_part_menu "${path_conf}wg${num_conf}.conf"
sleep 1

echo -e "pour lancer le tunnel :"
sleep 0.5
echo_step_menu "wg-quick up wg${num_conf}"
sleep 1

        echo_ko_menu " ---- en cas de dysfonctionnement ----"
sleep 1
        echo "Pour assigner l'adresse a la nouvelle interface :"
sleep 0.5
        echo_step_menu "ip address add ${plage} dev wg${num_conf}"
sleep 0.5
        echo_step_menu "ip -6 address add ${plage6} dev wg${num_conf}"
sleep 1
        echo "Pour assigner une nouvelle route :"
sleep 0.5
        echo_step_menu "ip route add 10.0.0.1 dev wg${num_conf}"
sleep 0.5
        echo_step_menu "post-up ip -6 route add   fd64:ba10:cafe:c0ca::1 dev wg${num_conf}"
sleep 1

conf_server='
# wg'${num_conf}' sur '${machine}' IPv4 & IPv6\n
[Peer]\n
PublicKey ='${pub}'\n
PresharedKey ='${pre}'\n
AllowedIPs = '${plage}', '${plage6}'\n '

echo -e ${conf_server} > ${path_archive}wg${num_conf}-${machine}.baionet
chmod 600 ${path_archive}wg${num_conf}-${machine}.baionet
echo "Fichier a transmettre a baionetek@framalistes.org"
sleep 1
sleep 0.5
echo_part_menu "${path_archive}wg${num_conf}-${machine}.baionet"
echo "###### contenu de wg${num_conf}-${machine}.baionet #################################"
sleep 0.5
echo_ok_menu ${conf_server}
sleep 0.5
echo "###### Fin du contenu a transmettre #################################"
