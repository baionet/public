# Wireguard

## client

scripts à executer pour les clients

### wgConfYunohost.sh

A executer sur sa propre machine. 
Génère conf client et conf serveur dans un sous-répertoire.

### wireguard.sh

C'est simplement une copie de wgconfclient4.sh. Il existe pour simplifier la vie des gens qui viennent du blog et pour wget le fichier
* todo : faire une url simple

### wgconfclient4.sh

* install de wireguard
* genere les clefs et le secret
* propose une conf /etc/wireguard/wgX.conf
* propose une interface /etc/network/interface.d/wgXpropose une conf 

### wgconfclient6.sh

Destiné aux adhérents avec simple vm IPv6
En cours de dev. 

## Serveur

Scripts à executer sur le serveur (dev en cours)

### create_new_wireguard_account

Fichier à placer sous /usr/local/bin/
Environnement python

### new_wireguard_client

Fichier à placer sous /usr/local/bin/
bash

### new_wireguard_adherent_ipv6

script pour générer les confs ipv6 only 
En cours de dev

