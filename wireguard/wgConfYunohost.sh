# !/bin/bash
# Script tested under debian bullseye
# no garantee !
# from pizzacoca 
# GPLv3

########## Gestion de l'affichage couleur #############
function echo_part_menu() { echo -e "\e[0;32m${*}\e[m"; }
function echo_step_menu() { echo -e "\e[0;36m${*}\e[m"; }
function echo_ok_menu() { echo -e "\e[1;32m${*}\e[m"; }
function echo_ko_menu() { echo -e "\e[1;31m${*}\e[m"; }



########## Infos serveur #############
endpoint="80.67.186.254:51820"
server_pubkey="pmcbNukQLOFSdImt2Tzy5gG4NLDOxHGI4MYGugBKWAE="

########## Infos client#############

# systeme  client
systeme="yunohost"
# répertoire de sauvegarde
path_conf="./conf-wireguard/"

if [ ! -d ${path_conf} ];then
	echo "Creation du dossier ${path_conf} ";
	mkdir ${path_conf}
fi

#plages par défaut
plageDefault="80.67.186.27/32"
plage6Default="2001:913:5201:27::27/128"

# demande du nom du client
clientDefaut="test"

read -p "nom du ${systeme} ? [$clientDefaut] : " client
if [[ ${client} -eq "" ]];then
	client=$clientDefaut
	echo_ko_menu "Votre ${systeme} s'appelle par défaut ${client}"
else 
	echo_ok_menu "Votre ${systeme} s'appelle ${client}"

fi
sleep 0.2

fileClient=$client"-client.conf"
echo $fileClient
sleep 0.2
fileServer=$client"-server.conf"
echo $fileServer


# demandes des adresses ip
read -p "quelle est l'adresse IpV4 souhaitée ?  [$plageDefault] " plage
if [[ ${plage} -eq "" ]];then
	plage=$plageDefault;
	echo_ko_menu "Valeur par défaut ${plage}"
else 
	echo_ok_menu "Valeur ${plage} choisie"
fi
read -p "quelle est l'adresse IpV6 souhaitée ?  [$plage6Default] " plage6
if [[ ${plage6} -eq "" ]];then
	plage6=$plage6Default;
	echo_ko_menu "Valeur par défaut ${plage6}"
else 
	echo_ok_menu "Valeur ${plage6} choisie"

fi
#####

sleep 0.2

##################Génération des clefs 

privateWg=${systeme}"-"${client}"-private.key"
publicWg=${systeme}"-"${client}"-public.key"
secretWg=${systeme}"-"${client}"-shared-secret.key"

umask 077
wg genkey | tee ${path_conf}${privateWg} | wg pubkey > ${path_conf}${publicWg}
wg genpsk > ${path_conf}${secretWg}
umask 0022
echo_step_menu "clef privée enregistrée sous ${path_conf}${privateWg}"
sleep 0.2
echo_step_menu "clef publique enregistrée sous ${path_conf}${publicWg}"
sleep 0.2
echo_step_menu "secret partagé enregistré sous ${path_conf}${secretWg}"
sleep 0.2

pre=$(cat ${path_conf}${secretWg})
priv=$(cat ${path_conf}${privateWg})
pub=$(cat ${path_conf}${publicWg})

sleep 0.2

############## configuration wireguard pour le client
conf_client='
# Conf wireguard sur '${systeme}' "'${client}'" IPv4 & IPv6 \n
# fichier à transférer sur le client '${client}'\n
[Interface]\n
PrivateKey = '${priv}'\n
Address = '${plage}'\n
Address = '${plage6}'\n
MTU = 1300\n
\n
[Peer]\n
PublicKey ='${server_pubkey}'\n
Endpoint = '${endpoint}'\n
PresharedKey ='${pre}'\n
AllowedIPs = 0.0.0.0/0, ::/0\n
PersistentKeepalive = 25\n
	'

echo -e ${conf_client} > ${path_conf}${fileClient}
#echo_part_menu "${path_conf}${fileClient}"
sleep 0.5

###########Configuration pour le serveur de baionet
conf_server='
# wg'${num_conf}' sur  '${systeme}' "'${client}'" IPv4 & IPv6\n
[Peer]\n
PublicKey ='${pub}'\n
PresharedKey ='${pre}'\n
AllowedIPs = '${plage}', '${plage6}'\n
'

echo -e ${conf_server} > ${path_conf}${fileServer}
chmod 600 ${path_conf}${fileServer}

echo_ok_menu "Fichier ${path_conf}${fileClient} a transférer au client :"
echo -e ${conf_client}
sleep 0.5
echo_ok_menu "Fichier ${path_conf}${fileServer} a transférer à baionet :"
echo -e ${conf_server}


